package dao;
import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pojo.Singer;
import utils.dbUtils;

public class TestSinger implements Closeable {

		Connection connection = null;
		Statement statement = null;
		public TestSinger() throws Exception{
			 this.connection = dbUtils.getConnection();
			 this.statement = this.connection.createStatement();
		}
		
		public List<Singer> displaySingers( )throws Exception{
			String sql = "SELECT * FROM Singers";
			List<Singer> Singers = new ArrayList<Singer>();
			try( ResultSet rs = this.statement.executeQuery(sql) ){
				while( rs.next())
				{
					Singer singer = new Singer(rs.getString("name"),rs.getString("gender"),rs.getString("email_id"),rs.getString("contact"),rs.getInt("age"),rs.getInt("rating"));
				Singers.add( singer );
			}
			return Singers;
		}
	}     // **Write a class Singer[name,gender,email_id,contact,rating,age]**
		// 	public Singer(String name, String gender, String email_id, String contact, int age, int rating) {
		public int addSinger(Singer singer) throws Exception{
			String sql = "INSERT INTO Singers VALUES('" + singer.getName()+"','"+singer.getGender()+"','"+singer.getEmail_id()+"','"+singer.getContact()+"',"+singer.getRating()+","+singer.getAge()+")";
			return this.statement.executeUpdate(sql);
		}
		
		public int removeSinger(String email_id) throws Exception{
			String sql = "DELETE FROM Singers WHERE email_id= '"+email_id+"'";
			return this.statement.executeUpdate(sql);
		}
		
		public int modifyRating(String email_id, int newRating) throws Exception{
			String sql = "UPDATE Singers SET rating="+newRating+" WHERE email_id='"+email_id+"'";
			return this.statement.executeUpdate(sql);
		}
		
		public Singer searchSinger(String name,List<Singer> list)
		{
			if(list != null)
			{
				for(Singer singer : list)
				{
					if(singer.getName().equals(name))
					{
						return singer; 
					}
				}
				
			}
			
			
			return null;
		}
		
		@Override
		public void close() throws IOException {
			try {
				connection.close();
			} catch (SQLException cause) {
				throw new IOException(cause);
			}
		}
	}