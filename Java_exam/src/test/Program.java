package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import dao.TestSinger;
import pojo.Singer;

public class Program {
	static Scanner sc = new Scanner(System.in);
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Add Singer");
		System.out.println("2.Remove Singer ");
		System.out.println("3.Modify Rating");
		System.out.println("4.Search Singers with name from Collection");
		System.out.println("5.Display Singers");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	
	
	

	public static void main(String[] args) {
	
		int choice;
	
		while( ( choice = Program.menuList( ) ) != 0 ) {
			switch( choice ) {
			case 1:	
				
				try (TestSinger test = new TestSinger()){
					
					System.out.println("please enter Singer name ");
					
				   String name = null;
				   
				 name = sc.next(); 
				   
				   sc.nextLine(); 
				   System.out.println("please enter Singer gender");
				   String	gender = sc.nextLine();
				  
				   System.out.println("please enter Singer email id ");
				   String	email_id = sc.nextLine();
				   
				   System.out.println("please enter Singer contact no");
				   String	contact = sc.nextLine();
				   
				   System.out.println("please enter Singer rating");
				 Integer rating = Integer.parseInt(sc.nextLine());
				 System.out.println("please enter Singer age ");
				 Integer age = Integer.parseInt(sc.nextLine());
					
				 Singer singer = new Singer(name,gender,email_id, contact, age, rating);
					
					//Singer singer = new Singer("suraj", "male", "s@gmail.com", "9881", 26, 5);
					
				int count = 	test.addSinger(singer); 
				System.out.println(count+" row(s) affected");
					
				}
					
				 catch (Exception ex) {
					 ex.printStackTrace();
				}
				
				break;
			case 2:	
                 try (TestSinger test = new TestSinger()){
					
                System.out.println("please enter email id of singer to remove");
   				   String	email_id = sc.next();
   				   sc.nextLine(); 
                	 
			//	int count = 	test.removeSinger("s@gmail.com"); 
   				int count = 	test.removeSinger(email_id); 
				System.out.println(count+" row(s) affected");
					
				}
					
				 catch (Exception ex) {
					 ex.printStackTrace();
				}
				
				break;
			case 3:	
				try (TestSinger test = new TestSinger()){
					
					  System.out.println("please enter Singers email id, whose rating is to be modified  ");
					   String	email_id = sc.next();
					sc.nextLine();
					
					  System.out.println("please enter Singer new rating between 1 to 10");
						 Integer rating = Integer.parseInt(sc.nextLine());
						
						 
						 int count = 	test.modifyRating(email_id,rating);  
						 
					//int count = 	test.modifyRating("suraj@gmail.com", 10);  
					System.out.println(count+" row(s) affected");
						
					}
						
					 catch (Exception ex) {
						 ex.printStackTrace();
					}
				break;
			case 4:
                try (TestSinger test = new TestSinger()){
                	Singer findSinger	= null;
                	List<Singer> singers =	test.displaySingers();
                	
                	System.out.println("please enter Singer name to find in database");
                	
                	String nameToFind = sc.next();
                	sc.nextLine();
				   findSinger =  test.searchSinger(nameToFind,singers );
                
				   if(findSinger != null)
				   {
					   System.out.println(findSinger.toString());
				   }else
				   {
					   System.out.println("singer with name "   + nameToFind +  " not found");
				   }
				
			
					}
						
					 catch (Exception ex) {
						 ex.printStackTrace();
					}
				
				
				break;
				
			case 5: 
            try (TestSinger test = new TestSinger()){
					
				List<Singer> singers =	test.displaySingers();
                	
				System.out.println("Singer List");
			  for(Singer singer : singers)
			  {
				  
				  System.out.println(singer.toString());
			  }
				
				
					}
						
					 catch (Exception ex) {
						 ex.printStackTrace();
					}
				break;
			}
		
		
		
		}
	}
}
